// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBKrJZGWW-oHSGAxKc68wXyjOP2y2PfGsQ",
    authDomain: "naranja-x-css-go.firebaseapp.com",
    projectId: "naranja-x-css-go",
    storageBucket: "naranja-x-css-go.appspot.com",
    messagingSenderId: "932675390565",
    appId: "1:932675390565:web:fa6ed0ea0c480b6fb98a8c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

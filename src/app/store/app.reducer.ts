import { ActionReducerMap } from '@ngrx/store';
import * as ui from '../shared/store/ui.reducer';
import * as auth from '../auth/store';
import * as order from '../page-modules/order/store/order.reducer';
export interface AppState {
   ui: ui.UiState,
   auth: auth.AuthState,
   order: order.OrderState
}

export const appReducers: ActionReducerMap<AppState> = {
   ui: ui.uiReducer,
   auth: auth.authReducer,
   order: order.orderReducer
}

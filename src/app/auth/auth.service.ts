import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { Observable, of, Subscription } from 'rxjs';
import { tap, delay, map } from 'rxjs/operators';
import { User } from '../models';
import { user } from '../interfaces';
import { Store } from '@ngrx/store';
import { AppState } from '../store';
import * as authActions from './store/auth.actions';
import { Router } from '@angular/router';
import { LocalStorageService } from '../utils/services';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private userSubscription: Subscription | undefined;

  // Guardar la URL para poder redireccionar despues de logearse
  public redirectUrl: string | null = null;

  constructor(
    public auth: AngularFireAuth,
    private firestore: AngularFirestore,
    private store: Store<AppState>,
    private router: Router,
    private localStorageService: LocalStorageService
  ) { }

  initAuthListener() {
    this.auth.authState.subscribe(fuser => {
      if (fuser) {
        this.userSubscription = this.firestore.doc(`${fuser.uid}/user`).valueChanges()
          .subscribe((firestoreUser: any) => {
            const user = User.fromFirebase(firestoreUser)
            this.store.dispatch(authActions.setUser({ user: user }));
            if (!!this.redirectUrl) this.router.navigateByUrl(this.redirectUrl);
            this.redirectUrl = null;
          })
      } else {
        this.userSubscription?.unsubscribe();
        this.store.dispatch(authActions.unsetUser());
      }
    });
  }

  logIn(email: string, password: string, rememberUser: boolean) {
    return this.auth.signInWithEmailAndPassword(email, password).then(() => {
      if (rememberUser) this.localStorageService.rememberUser();
    });
  }

  logOut() {
    return this.auth.signOut().then(() => this.localStorageService.removeUser());
  }

  registerUser(email: string, password: string) {
    return this.auth.createUserWithEmailAndPassword(email, password)
      .then(({ user }) => {
        if (user && user.uid && user.email) {
          const newUser = new User(user.uid, user.email);
          this.firestore.doc(`${user.uid}/user`).set({ ...newUser });
        }
      })
  }

  isAuth() {
    return this.auth.authState.pipe(
      map(fbUser => fbUser != null)
    );
  }
}

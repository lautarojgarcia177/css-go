import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  NavigationExtras,
  CanLoad, Route
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { AuthState } from './store';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, routerSnapshot: RouterStateSnapshot): Observable<boolean> {

    return this.checkLogin(routerSnapshot.url)
  }

  canActivateChild(route: ActivatedRouteSnapshot, routerSnapshot: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, routerSnapshot);
  }

  canLoad(route: Route): Observable<boolean> {
    const url = `/${route.path}`;

    return this.checkLogin(url);
  }

  checkLogin(url: string): Observable<boolean> {

    return this.authService.isAuth()
      .pipe(
        tap(isAuth => {
          if (!isAuth) {
            //No esta logeado, guardar la url y redirigir a login
            this.authService.redirectUrl = url;
            this.router.navigate(['auth', 'login']);
            return of(false);
          } else {
            return of(true);
          }
        })
      )
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { SweetalertService } from 'src/app/shared/sweetalert/sweetalert.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword: ['', RxwebValidators.compare({fieldName:'password' })],
  });

  constructor(private authService: AuthService, private fb: FormBuilder, private sweetalertService: SweetalertService, private router: Router) { }

  ngOnInit(): void {
  }

  registerUser() {
    this.authService.registerUser(this.registerForm.controls.email.value,this.registerForm.controls.password.value)
      .then(() => {
        this.sweetalertService.fireSuccess('Usuario registrado con éxito');
        setTimeout(() => {
          this.router.navigate(['']);
        }, 2000)
      })
      .catch(() => {
        this.sweetalertService.fireError('Error al registrar el usuario');
      });
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.registerUser();
    } else {
      this.sweetalertService.fireError('Formulario invalido, verifique los campos');
    }
  }

}

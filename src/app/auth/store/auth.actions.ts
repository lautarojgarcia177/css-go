import { createAction, props } from "@ngrx/store";
import { User } from "src/app/models";

export const logIn = createAction('[Auth] LogIn');
export const setUser = createAction('[Auth] setUser', props<{user: User}>());
export const logOut = createAction('[Auth] LogOut');
export const unsetUser = createAction('[Auth] unsetUser');

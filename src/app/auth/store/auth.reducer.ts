import { Action, createReducer, on } from "@ngrx/store"
import { User } from "src/app/models";
import * as authActions from "./auth.actions"

export interface AuthState {
  user: User | null;
}

export const initialAuthState: AuthState = {
  user: null
}

const _authReducer = createReducer(
  initialAuthState,
  on(authActions.setUser, (state, {user}) => ({...state, user: {...user}})),
  on(authActions.unsetUser, (state) => ({...state, user: null})),
);

export function authReducer(state: AuthState = initialAuthState, action: Action): AuthState {
  return _authReducer(state, action);
}

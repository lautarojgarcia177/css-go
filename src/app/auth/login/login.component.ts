import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthState } from '../store/auth.reducer';
import * as authActions from '../store/auth.actions';
import { AuthService } from '../auth.service';
import { AppState } from 'src/app/store';
import { tap } from 'rxjs/operators';
import * as uiActions from 'src/app/shared/store/ui.actions';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { SweetalertService } from 'src/app/shared/sweetalert/sweetalert.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    rememberUser: [''],
  });

  isLoading: boolean = false;
  private isLoading$ = this.store.select('ui').pipe(tap(ui => this.isLoading = ui.isLoading));
  private isLoadingSubscription: Subscription | undefined;

  constructor(
    private store: Store<AppState>,
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private sweetalertService: SweetalertService) { }

  ngOnInit(): void {
    this.isLoadingSubscription = this.isLoading$.subscribe();
  }

  ngOnDestroy(): void {
    this.isLoadingSubscription?.unsubscribe();
  }

  onSubmit() {
    this.login();
  }

  public login(): void {

    this.store.dispatch( uiActions.isLoading() );

    this.authService.logIn(this.loginForm.controls.email.value,this.loginForm.controls.password.value, this.loginForm.controls.rememberUser.value)
    .then(() => {
      this.store.dispatch(uiActions.stopLoading());
      this.router.navigate([''])
    })
    .catch(err => {
      console.error('Login error', err)
      this.store.dispatch(uiActions.stopLoading());
      this.sweetalertService.fireError('Error de login, verifique los datos ingresados');
    });
  }

  public logout(): void {
    this.authService.logOut()
    .then(() => {})
    .catch(err => {
      console.error('Logout error', err)
      this.store.dispatch(uiActions.stopLoading());
    });
  }

}

import { Injectable, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/store';
import * as orderActions from '../store/order.actions';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService implements OnDestroy {

  private cart$ = this.store.select('order');
  private cartSubscription: Subscription | undefined;

  constructor(private store: Store<AppState>, private firestore: AngularFirestore) { }

  ngOnDestroy() {
    if (!!this.cartSubscription) this.cartSubscription.unsubscribe();
  }

  public confirmOrder(): void {
    this.cartSubscription = this.cart$.subscribe(orderState => {
      const cart = orderState.cart;
      // this.firestore.doc(`orders`).set()
      this.cartSubscription?.unsubscribe();
      this.store.dispatch(orderActions.emptyCart());
    });
  }
}

import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Menu } from 'src/app/models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private firestore: AngularFirestore) { }

  getAllMenus(): Observable<Menu[]> {
    return this.firestore.collection('menus').valueChanges()
    .pipe(
      // map(querySnapshot => querySnapshot.docs.map(doc => doc.data() as Menu))
      map(docs => docs.map(col => col as Menu))
    );
  }

}

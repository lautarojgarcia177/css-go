import { createAction, props } from "@ngrx/store";
import { Menu } from "src/app/models";

export const addToCart = createAction('[Order] AddToCart', props<{menu: Menu, quantity: number}>());
export const removeFromCart = createAction('[Order] RemoveFromCart', props<{menu: Menu, quantity: number}>());
export const emptyCart = createAction('[Order] EmptyCart');

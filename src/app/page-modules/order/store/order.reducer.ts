import { Action, createReducer, on } from "@ngrx/store"
import { Menu } from "src/app/models";
import * as orderActions from "./order.actions"
import * as _ from 'lodash';

export interface OrderState {
  cart: CartEntry[] | null;
}

export const initialOrderState: OrderState = {
  cart: []
}

export interface CartEntry {
  menu: Menu,
  quantity: number
}

function addToCart(cart: CartEntry[], cartAddition: CartEntry): CartEntry[] {
  /* Verifica si el menu ya existe en el carrito le agrega mas cantidad */
  const _cart = _.cloneDeep(cart);
  for (let i = 0; i < _cart.length; i++) {
    if (_cart[i].menu.name === cartAddition.menu.name) {
      _cart[i].quantity += cartAddition.quantity;
      return _cart;
    }
  }
  /* Si llego hasta aca es que el menu no existe en el carrito, lo agregamos */
  _cart.push(cartAddition);
  return _cart;
}

function removeFromCart(_cart: CartEntry[], cartAddition: CartEntry): CartEntry[] {
  const cart = _.cloneDeep(_cart);
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].menu.name === cartAddition.menu.name) {
      cart[i].quantity -= cartAddition.quantity;
      if (cart[i].quantity <= 0) cart.splice(i,1)
      return cart;
    }
  }
  return cart;
}

const _orderReducer = createReducer(
  initialOrderState,
  on(orderActions.addToCart, (state: any, cartAddition) => ({...state, cart: addToCart(state.cart, cartAddition)})),
  on(orderActions.removeFromCart, (state: any, cartRemoval) => ({...state, cart: removeFromCart(state.cart, cartRemoval)})),
  on(orderActions.emptyCart, (state: any) => ({...state, cart: []})),
);

export function orderReducer(state: OrderState = initialOrderState, action: Action): OrderState {
  return _orderReducer(state, action);
}

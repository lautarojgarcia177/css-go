import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './pages/order/order.component';
import { OrderRoutingModule } from './order-routing.module';
import { SharedModule } from 'src/app/shared';
import { MenuComponent } from './components/menu/menu.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable'


@NgModule({
  declarations: [
    OrderComponent,
    MenuComponent,
    CheckoutComponent
  ],
  imports: [
    OrderRoutingModule,
    SharedModule,
    NgxDatatableModule
  ]
})
export class OrderModule { }

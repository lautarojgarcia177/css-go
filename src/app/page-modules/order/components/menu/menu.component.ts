import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Menu } from 'src/app/models/menu.model';
import { SweetalertService } from 'src/app/shared/sweetalert/sweetalert.service';
import { AppState } from 'src/app/store';
import * as orderActions from '../../store/order.actions';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public quantity: number = 0;

  @Input() menu: Menu = new Menu();

  constructor(private store: Store<AppState>, private sweetalertService: SweetalertService) { }

  ngOnInit(): void {
  }

  public addToCart(): void {
    this.store.dispatch(orderActions.addToCart({ menu: this.menu, quantity: this.quantity}));
    this.quantity = 0;
    this.sweetalertService.fireSuccess('Producto/s añadido/s al carrito');
  }


}

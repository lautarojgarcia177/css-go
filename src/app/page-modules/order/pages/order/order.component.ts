import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { SweetalertService } from 'src/app/shared/sweetalert/sweetalert.service';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.sass']
})
export class OrderComponent implements OnInit {

  menus$ = this.menuService.getAllMenus();

  constructor(private authService: AuthService, private router: Router, private menuService: MenuService, private sweetalertService: SweetalertService) { }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logOut()
      .then(() => this.router.navigate(['login']));
  }

  public fireSWAL() {
    this.sweetalertService.fireError('hola');
  }

}

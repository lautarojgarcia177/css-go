import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { Menu } from 'src/app/models';
import { SweetalertService } from 'src/app/shared/sweetalert/sweetalert.service';
import { AppState } from 'src/app/store';
import { CartEntry, OrderState } from '../../store/order.reducer';
import * as orderActions from '../../store/order.actions';
import { CheckoutService } from '../../services/checkout.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit, OnDestroy {

  private cart$ = this.store.select('order');
  private cartSubscription: Subscription | undefined;
  public cart: any = null;

  cancelBtn: boolean = false;

  constructor(private store: Store<AppState>, private sweetalertService: SweetalertService, private checkoutService: CheckoutService) {}

  ngOnInit(): void {
    this.cartSubscription = this.cart$.subscribe((orderState: OrderState) => {
      if (!!orderState.cart) {
        this.cart = _.cloneDeep(orderState.cart);
        /* Calcular el total del carrito */
        this.cart.total = orderState.cart.reduce(
          (acc, curr) => {
            let subtotal = 0;
            if (!!curr && !!curr.menu && !!curr.menu.price) {
              subtotal = curr?.menu?.price * curr.quantity;
            }
            return acc + subtotal;
          },
          0
          )
        }
      });

    }

    submitButton() {
      if( this.cart[0] === undefined  ) {
        Swal.fire({
          title: 'Error',
          text: 'No dispone de comida en su carrito',
          confirmButtonColor: 'red',
          confirmButtonText: 'Aceptar',
          showCancelButton: false,
          allowEscapeKey: true,
          allowOutsideClick: false
        })
      } else {
        Swal.fire({
          title: '¡Pedido Confirmado!',
          text: 'Su pedido se realizó con éxito',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Aceptar',
          showCancelButton: false,
          allowEscapeKey: true,
          allowOutsideClick: false
        }).then(() => {
          location.reload()
      });
      }
  }


  ngOnDestroy() {
    this.cartSubscription?.unsubscribe();
  }

  confirmOrder(): void {
    this.sweetalertService.fireSuccess('Pedido confirmado con éxito');
    this.checkoutService.confirmOrder();
  }

  public removeFromCart(menu: Menu, quantity: number) {
    this.store.dispatch(orderActions.removeFromCart({menu, quantity}));
    this.sweetalertService.fireSuccess('Menu eliminado del carrito');
  }

}

/* Archivo de barrel para tener un solo punto de importacion/exportacion */

export { AdminModule } from './admin/admin.module';
export { OrderModule } from './order/order.module';
export { MapModule } from './map/map.module';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthService } from 'src/app/auth/auth.service';
import { AppState } from 'src/app/store';
import * as uiActions from 'src/app/shared/store/ui.actions';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export default class NavbarComponent implements OnInit {

  items = [
    { name: 'Ordenar', path: '/order' },
    // { name: 'Admin', path: '/admin' },
  ];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private authService: AuthService, private store: Store<AppState>) { }

  ngOnInit(): void {
    // this.activatedRoute.
  }

  public logOut(): void {
    this.authService.logOut()
    .then(() => {
      this.router.navigate(['login']);
    })
    .catch(err => {
      console.error('Logout error', err)
      this.store.dispatch(uiActions.stopLoading());
    });
  }

}

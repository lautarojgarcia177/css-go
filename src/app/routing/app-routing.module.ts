import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../auth/auth.guard';
import { FooterComponent } from './footer/footer.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SelectivePreloadingStrategyService } from './selective-preloading-strategy.service';


const appRoutes: Routes = [
  {
    path: 'order',
    loadChildren: () => import('../page-modules').then(m => m.OrderModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'admin',
    loadChildren: () => import('../page-modules').then(m => m.AdminModule),
    canLoad: [AuthGuard]
  },
  // {
  //   path: 'checkout',
  //   loadChildren: () => import('../page-modules').then(m => m.ChatModule),
  //   canLoad: [AuthGuard]
  // },
  {
    path: 'map',
    loadChildren: () => import('../page-modules').then(m => m.MapModule),
  },
  {
    path: 'auth',
    loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule),
  },
  { path: '',   redirectTo: '/order', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    PageNotFoundComponent,
    FooterComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false, // <-- debugging purposes only
        preloadingStrategy: SelectivePreloadingStrategyService,
      }
    )
  ],
  exports: [
    RouterModule,
    FooterComponent
  ]
})
export class AppRoutingModule { }

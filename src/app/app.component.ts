import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from './auth/auth.service';
import { SweetalertService } from './shared/sweetalert/sweetalert.service';
import { AppState } from './store';
import { LocalStorageService } from './utils/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  public showNavbar = false;

  constructor(
    translate: TranslateService,
    authService: AuthService,
    store: Store<AppState>,
    spinner: NgxSpinnerService,
    localStorageService: LocalStorageService) {
    /* Translate */
    translate.setDefaultLang('en');
    translate.use('es');
    /* Auth */
    authService.initAuthListener();
    localStorageService.checkLocalStorageUser();
    store.select('auth').subscribe(auth => !!auth.user ? this.showNavbar = true : this.showNavbar = false);
    /* Loading Spinner */
    store.select('ui').subscribe(ui => ui.isLoading ? spinner.show() : spinner.hide());
  }
}

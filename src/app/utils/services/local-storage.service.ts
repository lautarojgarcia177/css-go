import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/store';
import * as authActions from 'src/app/auth/store/auth.actions';
import { User } from 'src/app/models';
import { user } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private auth$ = this.store.select('auth');
  private authSubscription: Subscription | undefined;
  private storageLoggedUserKeyName = 'LoggedUser';

  constructor(private store: Store<AppState>) { }

  ngOnDestroy() {
  }

  public setItem(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  public getItem(key: string): any {
    return localStorage.getItem(key)
  }

  public removeItem(key:string): void {
    localStorage.removeItem(key);
  }

  public clear(): void {
    localStorage.clear();
  }

  public checkLocalStorageUser(): void {
    /* Verifica si hay un usuario logeado en localStorage */
    const storageLoggedUser = this.getItem(this.storageLoggedUserKeyName);
    if(!!storageLoggedUser) {
      let loggedUser = JSON.parse(storageLoggedUser) as user;
      loggedUser = new User(loggedUser.uid, loggedUser.email);
      this.store.dispatch(authActions.setUser({user: loggedUser}))
    }

  }

  public rememberUser(): void {
    /* Guardar usuario logeado en localstorage */
    this.authSubscription = this.auth$.subscribe(auth => {
      if (!!auth.user) {
        this.setItem(this.storageLoggedUserKeyName, JSON.stringify(auth.user));
      } else {
        this.removeItem(this.storageLoggedUserKeyName);
      }
      /* Elimina la suscripcion para prevenir problemas */
      this.authSubscription?.unsubscribe();
    });
  }

  public removeUser(): void {
    this.removeItem(this.storageLoggedUserKeyName);
  }

}

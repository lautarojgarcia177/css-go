import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { tap } from 'rxjs/operators';
import { UiState } from '../shared/store/ui.reducer';
import * as uiActions from '../shared/store/ui.actions';
import { Store } from '@ngrx/store';

@Injectable()
export class SpinnerHttpInterceptor implements HttpInterceptor {

  constructor(private store: Store<UiState>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(
        event => event instanceof HttpResponse ? this.store.dispatch(uiActions.stopLoading()) : this.store.dispatch(uiActions.isLoading()),
        () => this.store.dispatch(uiActions.stopLoading())
      )
    );
  }

}

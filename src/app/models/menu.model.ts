export class Menu {

  name?: string;
  description?: string;
  price?: number;
  imgUrl?: string;

  constructor(obj?: Partial<Menu>) {
    this.name = '';
    this.description = '';
    this.price = -1;
    this.imgUrl = '';
    Object.assign(this, obj);
  }
}

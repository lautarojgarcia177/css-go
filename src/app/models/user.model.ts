export class User {

  static fromFirebase({uid, email}: {uid: string, email: string}) {
    return new User(uid,email);
  }

  constructor(
    public uid: string,
    public email: string
  ) {}
}

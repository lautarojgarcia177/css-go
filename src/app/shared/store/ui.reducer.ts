import { createReducer, on } from '@ngrx/store';
import * as uiActions from './ui.actions';

export interface UiState {
  isLoading: boolean;
}

export const initialState: UiState = {
  isLoading: false
}

const _uiReducer = createReducer(
  initialState,
  on(uiActions.isLoading, state => ({ ...state, isLoading: true })),
  on(uiActions.stopLoading, state => ({ ...state, isLoading: false })),
);

export function uiReducer(state: any, action: any) {
  return _uiReducer(state, action);
}

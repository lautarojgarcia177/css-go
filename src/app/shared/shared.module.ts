/* Modulo con features compartidas entre todos los modulos */

import { ModuleWithProviders, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';

//Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

// translations
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

// SweetAlert2
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { SweetalertComponent } from './sweetalert/sweetalert.component';

// Ng-bootstrap
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from '../routing/footer/footer.component';


@NgModule({
  declarations: [
    SweetalertComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'es',
      isolate: false
    }),
    SweetAlert2Module.forChild(),
    NgbModule,
  ],
  exports: [
    SweetalertComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    TranslateModule,
    SweetAlert2Module,
    NgbModule
  ]
})
export class SharedModule {}

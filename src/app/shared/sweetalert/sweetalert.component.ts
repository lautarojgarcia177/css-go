import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Subscription } from 'rxjs';
import { SweetAlertOptions } from 'sweetalert2';
import { SweetalertService } from './sweetalert.service';

@Component({
  selector: 'app-sweetalert',
  templateUrl: './sweetalert.component.html',
  styleUrls: ['./sweetalert.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SweetalertComponent implements OnInit, OnDestroy {

  @ViewChild('swal') public readonly swal!: SwalComponent;

  public swalOptions: SweetAlertOptions = { };

  fireSuccessSubscription: Subscription | undefined;
  fireErrorSubscription: Subscription | undefined;

  constructor(private sweetalertService: SweetalertService, private changeDetectorRef: ChangeDetectorRef) {
    this.fireSuccessSubscription = sweetalertService.fireSuccess$.subscribe(
      message => {
        this.swalOptions = { text: message, icon: 'success', showConfirmButton: false, timer: 2000 };
        this.fireSwal();
      }
    )
    this.fireErrorSubscription = sweetalertService.fireError$.subscribe(
      message => {
        this.swalOptions = { text: message, icon: 'error', showConfirmButton: false, timer: 2000 };
        this.fireSwal();
      }
    )
   }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    // Prevenir memory leak cuando se destruye el componente
    this.fireSuccessSubscription?.unsubscribe();
    this.fireErrorSubscription?.unsubscribe();
  }

  private fireSwal(): void {
    // changeDetector para detectar los cambios antes de disparar
    this.changeDetectorRef.detectChanges();
    this.swal.fire();
  }

}

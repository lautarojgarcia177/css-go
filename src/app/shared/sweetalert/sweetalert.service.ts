import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SweetalertService {

  // Observable string sources
  private fireSuccessSource = new Subject<string>();
  private fireErrorSource = new Subject<string>();

  // Observable string streams
  fireSuccess$ = this.fireSuccessSource.asObservable();
  fireError$ = this.fireErrorSource.asObservable();

  constructor() { }

  // Comandos de mensaje del servicio
  public fireSuccess(message: string): void {
    this.fireSuccessSource.next(message);
  }
  public fireError(message: string): void {
    this.fireErrorSource.next(message);
  }

}
